#!/usr/bin/env ruby
# frozen_string_literal: true

require 'debug'
require 'csv'
require 'fileutils'
require 'dotenv/load'
require 'time'
require 'rubyXL'
require 'rubyXL/convenience_methods/cell'
require 'rubyXL/convenience_methods/workbook'
require 'rubyXL/convenience_methods/worksheet'
require 'libreconv'

TEMPLATE = '[NEW v2 TEMPLATE] GitLab Spain Monthly Timesheet.xlsx'
DEFAULT_TEMPLATE_NAME = 'Monthly Timesheet EN'
MONTH = ENV['MONTH'] ? Date::MONTHNAMES.index(ENV['MONTH']) : Date.today.prev_month.month
MONTH_NAME = TEMPLATE_NAME = Date::MONTHNAMES[MONTH]
YEAR = ENV['YEAR'] || Date.today.prev_month.year
DEFAULT_CLOCK_IN = ENV['DEFAULT_CLOCK_IN']
DEFAULT_CLOCK_OUT = ENV['DEFAULT_CLOCK_OUT']
DEFAULT_BREAK = ENV['DEFAULT_BREAK']
EMPLOYEE_NAME = ENV['EMPLOYEE_NAME']
MANAGER = ENV['MANAGER']
TIMESHEET = "Timesheet-#{EMPLOYEE_NAME}-#{YEAR}-#{MONTH}.xlsx".freeze
TIMESHEET_PDF = "Timesheet-#{EMPLOYEE_NAME}-#{YEAR}-#{MONTH}.pdf".freeze
ROW_START = 9
OOO_TEXT = 'OOO'
@ooo_days = {}

def parse_ooo_row(row)
  return unless row[10] == 'Approved'

  (Date.parse(row[13])..Date.parse(row[14])).map do |d|
    next unless d.year == YEAR
    next unless d.month == MONTH

    @ooo_days[d.month] ||= []
    @ooo_days[d.month] << d.day
  end
end

def parse_ooo_csv(ooo_csv)
  csv = CSV.read(ooo_csv, headers: true)

  csv.each { |row| parse_ooo_row(row) }
end

def handle_ooo(month, day, row)
  return false unless @ooo_days[month]&.include?(day)

  update_cell_contents(row, 'G', OOO_TEXT)
end

def update_cell_contents(row, column, content, formula: true)
  cell = @sheet.cell_at("#{column}#{row}")
  cell.change_contents(content, formula ? cell.formula : "#{column}#{row}")
end

def delete_cell(columns, row)
  columns.each do |column|
    @sheet.cell_at("#{column}#{row}").remove_formula
    update_cell_contents(row, column, ' ')
  end
end

def remove_extra_days(last_day)
  ((last_day + 1)..31).each do |day|
    row = day + ROW_START
    delete_cell(%w[A B C D E F], row)
  end
end

def write_times(range_date)
  range_date.each do |date_day|
    row = date_day.day + ROW_START

    delete_cell(['B'], row)
    delete_cell(['C'], row)
    update_cell_contents(row, 'B', date_day.strftime('%A'), formula: true)
    update_cell_contents(row, 'C', date_day.day + 1, formula: false)
    update_cell_contents(row, 'D', Time.parse("#{date_day} 0:00}"))
    update_cell_contents(row, 'E', Time.parse("#{date_day} 0:00"))
    next if date_day.saturday? || date_day.sunday? || handle_ooo(date_day.month, date_day.day, row)

    update_cell_contents(row, 'D', Time.parse("#{date_day} #{DEFAULT_CLOCK_IN}"))
    update_cell_contents(row, 'E', Time.parse("#{date_day} #{DEFAULT_CLOCK_OUT}"))
    update_cell_contents(row, 'F', DEFAULT_BREAK)
  end

  remove_extra_days(range_date.last.day)
end

def update_sheet_metadata
  update_cell_contents(3, 'G', MONTH_NAME)
  update_cell_contents(5, 'G', YEAR)
  update_cell_contents(11, 'M', MONTH_NAME)
  update_cell_contents(11, 'N', YEAR)
  update_cell_contents(3, 'C', EMPLOYEE_NAME)
  update_cell_contents(5, 'C', MANAGER)
end

def process_month
  range_date = Date.new(YEAR, MONTH, 1)..Date.new(YEAR, MONTH, -1)

  workbook = RubyXL::Parser.parse(TIMESHEET)
  workbook[DEFAULT_TEMPLATE_NAME].sheet_name = TEMPLATE_NAME if workbook[DEFAULT_TEMPLATE_NAME]
  @sheet = workbook[TEMPLATE_NAME]

  update_sheet_metadata
  write_times(range_date)

  workbook.write(TIMESHEET)
end

ooo_csv = Dir['*OOO*.csv'].max_by { |f| File.ctime(f) }
raise Errno::ENOENT, '*OOO*.csv' unless ooo_csv

parse_ooo_csv(ooo_csv)
FileUtils.cp(TEMPLATE, TIMESHEET)

process_month

Libreconv.convert(TIMESHEET, TIMESHEET_PDF)

puts "Saved as #{TIMESHEET_PDF}."
