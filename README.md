# Autoclock

Tool to populate a timesheet automatically based on your established working hours and an OOO report from Time Off by Deel.

## Requirements

Ruby and Libreoffice (for PDF conversion)

## Getting started

1. Copy `.env.sample` into `.env` and modify the default config as you see fit
1. Go to `Time Off by Deel` on Slack, click on Reporting, and generate a YTD report with your matching user
1. Download the report and put it in this folder - as long as the name contains `OOO`, the latest file will be picked by the tool
1. Run `./autoclock.rb` and a new spreadsheet and PDF will appear named `Timesheet-NAME-YEAR-MONTH` with the data populated

Future runs will just require steps 2 to 4.

## TODO (the following is manual)

* Handle partial days

## License

MIT